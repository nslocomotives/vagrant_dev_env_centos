class repository {

  package { curl:
    ensure => 'installed',
    }

  # Install the GPG key
  exec { 'import-key':
    path    => '/bin:/usr/bin',
    command => 'curl http://repos.servergrove.com/servergrove-rhel-6/RPM-GPG-KEY-servergrove-rhel-6 -o /etc/pki/rpm-gpg/RPM-GPG-KEY-servergrove-rhel-6',
    require => Package['curl'],
  }
 
include epel
include stdlib

# PHP packages require EPEL to satisfy dependencies
#  exec { "epel.repo":
#    command => 'rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm',
#    path    => ['/bin', '/usr/bin'],
#    unless  => 'rpm -qa | grep epel',
#  }
 
  # Enable the repository
  yumrepo { 'servergrove.repo':
    baseurl  => 'http://repos.servergrove.com/servergrove-rhel-6/$basearch',
    enabled  => 1,
    gpgcheck => 1,
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-servergrove-rhel-6',
    require  => Exec['import-key']
  }
}

include apache
include apache::mod::php
include mysql::server


stage { pre: before => Stage[main] }
 
# Forces the repository to be configured before any other task
class { 'repository': stage => pre }

